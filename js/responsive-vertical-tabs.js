/**
 * @file
 * Responsive Vertical Tabs
 */
(function ($) {

  "use strict";

  /**
   * Remove the size attibut to make text fields respect CSS sizes
   */
  $('input[size]', 'div.vertical-tabs-panes').removeAttr('size');

}(jQuery));

document.addEventListener('click', $, false);
